![NotSoStupidCopier_v2](https://framagit.org/Gtn/notsostupidcopier_v2) is available 
It's quicker, but it doesn't implement profiles


# NotSoStupidCopier

NotSoStupidCopier (NSSCP) is a copier used to synchronise two folders.

It check whether a file is present in both folders, and if it not it copy it to the second folder.
It also check if a file was modified, and if it has been modified, it override the old file with the new file (can be disabled if you just want to synchronise files without date sync)

How to install ?
Download INSTALL.tar.gz archive, launch INSTALL file (without sudo)
Then, you can delete the downloaded file. 
To sync files, just open a terminal and type : *nsscp [first dir] [second dir]*
type *nsscp -h* to show help message
