/*
 * NotSoStupidCopier (NSSC) is an intelligent copier which synchronyse two directories
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "header/front_end.h"

structConfig front_end::parse_arguments(int argc, char *argv[]) {
    
    structConfig config = {"", "", false, false, false, false, false, false, std::vector<std::string>(), std::vector<std::string>(), 0};
    
//     config.error = 0;
//     config.backup = false;
//     config.checkFilenames = "";
//     config.help = false;
//     config.includeHidden = false;
//     config.noDateSync = false;
//     config.verbose = false;
//     config.command.push_back("");
    
    int state = 0;      

//     0 -> noting special
//     1 -> at least one Status command (except load) 
//     256 -> Error


    std::vector<std::string> args(argv + 1, argv + argc);
    
//     for(auto a:args) 
//         std::cout << a << std::endl; // Works well
    
    std::vector<std::string>::iterator argsIt(std::begin(args));
    
    while(argsIt != std::end(args)) {
//         std::cout << state << std::endl;
        if(argsIt->at(0) == '-') {
            
            if(argsIt->size() != 2) {
                config.error = 256;
                state = 256;
                break;
            } else {
                switch(argsIt->at(1)) {
                    case 'i':
                        config.includeHidden = true;
                        std::cout << "INFO : Include hidden mode" << std::endl; 
                        break;
                    case 'v':
                        config.verbose = true;
                        std::cout << "INFO : Verbose mode" << std::endl;
                        break;
                    case 'h':
                        config.help = true;
                        break;
                    case 'd':
                        config.noDateSync = true;
                        std::cout << "INFO : No date sync mode" << std::endl;
                        break;
                    case 'b':
                        config.backup = true;
                        std::cout << "INFO : backup mode" << std::endl;
                        break;
                    case 'c':
                        config.checkFilenames = true;
                        std::cout << "INFO : Checking for filenames compatibility" << std::endl; 
                        break;

                    case 'l':
                        if(args.size() >= std::distance(std::begin(args), argsIt) + 2) {
                            config.command.push_back("load");
                            config.arg.push_back(*(++argsIt));
//                             state = 1;
                            if(!addDir(&config, "--load")) {
                                state = 256;
                                config.error = 253;
                            }
                        } else {
                            state = 256;
                            config.error = 255;
                        }
                        break;
                    case 'p':
                        state = 1;
                        if(args.size() >= std::distance(std::begin(args), argsIt) + 2) {
                            if(*(++argsIt) == "save" && args.size() >= std::distance(std::begin(args), argsIt) + 3) {
                                config.command.push_back("save");
                                config.arg.push_back(*(++argsIt));
                                config.arg.push_back(*(++argsIt));
                                break;
                            }
                            if(*argsIt == "list") {
                                config.command.push_back("list");
                                break;
                            }
                            if(*argsIt == "remove"&& args.size() >= std::distance(std::begin(args), argsIt) + 2) {
                                config.command.push_back("remove");
                                config.arg.push_back(*(++argsIt));
                                break;
                            }
                            config.error = 255;
                            state = 256;
                        } else {
                            config.error = 255;
                            state = 256;
                        }
                        break;
                }
            }
        } else {
            // The first character of the arg is not "-"
            if(state == 1) {        // If a command is used (remove, ...)
                config.error = 254;
            }
            else {
                if(!addDir(&config, *argsIt)) {
                    config.error = 253;
                }
            }
        }
        argsIt++;
    }

    
    //     std::cout << "b" << config.noDateSync << std::endl;
    return config;
}


bool front_end::addDir(structConfig * config, std::string dir) {
    if(config->baseDirOne == ""){
        config->baseDirOne = dir;
        return true;
    }
    if(config->baseDirTwo == ""){
        config->baseDirTwo = dir;
        return true;
    }
    return false;
}


bool front_end::makeArguments(structConfig config) {

    
    switch(config.error) {
        case 256:
            front_end::showError("Found argument with more than one command character\n(for instance \"-iv\", instead of \"-i -v\")"); 
            break;
        case 255:
            front_end::showError("missing argument : \"-l\" and \"-c\" need one argument, \"-p\" need one (list), two (remove [profile]) or three (save [path] [name]) arguments");
            break;
        case 254:
            front_end::showError("Found non-compatibles arguments : \nYou cannot use -p option and synchronise directories in the same time");
            break;
        case 253:
            front_end::showError("Too much directories to synchronise\n(\"-l [profile]\" and \"/path/to/direcory\" without are view as directories to synchronise (except for \"-p save /path/to/direcory name\")");
            break;   
    }
                                        
    if(config.help) {
        front_end::showHelp();
    }

    if(config.help || config.error != 0) {
        return true;        
    }
    
    std::vector<std::string>::iterator actionsIt(std::begin(config.command)),
                                        actionsArgsIt(std::begin(config.arg));
    
//         std::cout << *actionsArgsIt << "|" << *(++actionsArgsIt) << std::endl;
//     actionsArgsIt--;
//     actionsArgsIt++;
    
    while(actionsIt != std::end(config.command)) {
        if(*actionsIt == "list")
            nsscp_core::listProfiles();
        
        
        if(*actionsIt == "save")
            nsscp_core::saveProfile(*(actionsArgsIt++), *(actionsArgsIt++));
        
        if(*actionsIt == "remove")
            nsscp_core::removeProfile(*(actionsArgsIt++));
        
        if(*actionsIt == "load") {
            if(config.baseDirOne == "--load") {
                config.baseDirOne = nsscp_core::loadProfile(*(actionsArgsIt++));
            } else {
                config.baseDirTwo = nsscp_core::loadProfile(*(actionsArgsIt++));
            }
        }

        actionsIt++;
    }  
    if(config.baseDirOne != "" && config.baseDirTwo != "") {
        nsscp_core syn;
//         std::cout << "c" << config.noDateSync << std::endl;
        syn.nsscp(config.baseDirOne, config.baseDirTwo, config.includeHidden, config.verbose, config.backup, config.checkFilenames, config.noDateSync);
    } else {
        if(config.command.size() == 0) {
            showError("Need at least two dir to synchronise or an action command");
        }
    }
    return true;    
}

void front_end::showError(std::string err) {
    std::cout << "ERROR :\n" << err << "\nType \"nsscp -h\" for help" << std::endl; 
    return;
}


void front_end::showHelp() {
    
    std::string help("NotSoStupidCopier - Beta 0.6");
	help += "\nFolder synchroniser - For USB Stick, or external disks...";
	help += "\nLook for file which were modified, or wihch are present in only one directory, and copy them";
    help += "\nUsage: nsscp [option] [FIRST DIRECORY PATH] [SECOND DIRECORY PATH]";
	help += "\n-i                        Include hidden files";
	help += "\n-v                        Verbose mode";
	help += "\n-h                        Show (this) help message";
	help += "\n-b                        Backup files (in the same direcory, with ~ before the name";
	help += "\n-c                        Check if the filenames are compatibility with other directory";
	help += "\n-d                        Do not syncrhonise modified files, only files not presents in both directories";
    help += "\n-l [\"profile name\"]       Load a path profile, so this option replace one the directories paths.";
	help += "\n                            Example : nsscp -l \"profile1\" /home/user";
	help += "\n                            With /media/user/usbstick saved in profile1, will by like :";
	help += "\n                            nsscp /media/user/usbstick /home/user";
    help += "\n-p [ list | save [name] [path] | remove [name] ]";
    help += "\n                          Profiles gestion : ";
	help += "\n                            list will list all profiles";
	help += "\n                            remove the profile [name]";
	help += "\n                            save will create a new profile named [name] and pointing to the path [path]";
//  help += "\nType man nsscp for more help";
	help += "\n\nby GtN";
    
    std::cout << help << std::endl;
    return;
}
