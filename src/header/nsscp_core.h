#ifndef DEF_NSSCP_CORE
#define DEF_NSSCP_CORE

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>
#include <unordered_map>
#include <tuple>


struct compatibility {
    std::string name;   
    std::string unallowed_char;
    std::vector<std::string> unallowed_word;
    long unsigned int max_lenght;
    bool end_space_allowed;
    compatibility() : name(""), unallowed_char(""),  unallowed_word(std::vector<std::string>()), max_lenght(0), end_space_allowed(true) {};
};

// #include "check_filenames.h"

class nsscp_core {
/**
 * @todo write docs
 * 
 * Use iterator instead of stupid ints in folderCopy
 * 
 */
    
public:
    nsscp_core();
    int nsscp(std::string _baseDirOne, std::string _baseDirTwo, bool _argIncludeHidden, bool _argVerbose, bool _argBackup, bool _argCheckFilenames, bool _argDateSync);
    
    int execCmd(std::string * command, bool redirectError);

    int folderSync();
    int fileSync();

    static bool removeProfile(std::string name);
    static bool listProfiles();
    static bool saveProfile(std::string path, std::string name);
    static std::string loadProfile(std::string name);

    std::string getUpperFolder(std::string * fold);

    bool notLast(unsigned short int * i, unsigned short int * j);
    void finishFoldCopy(unsigned short int *a, bool firstToSecond);

    bool checkDoNotSyncList(std::string dir);
    
    template<typename T>
    void out (T command, char type);

    void convertTextToMap(std::unordered_map<std::string, std::string> * map, std::ifstream * file);

    void fileDateSync(std::unordered_map<std::string, std::string>::iterator uIt, std::unordered_map<std::string, std::string>::iterator vIt, std::vector<std::string>::iterator dir);
    
//     bool check_compatibility(std::string fileOne, std::string fileTwo);
    
    std::vector<compatibility> loadCompatibility(std::string dirOne, std::string dirTwo);
    bool checkCompatibility(std::vector<compatibility> * cptb);
    std::string getFilesystemType(std::string * dir);

    bool checkNameCompatiblity(std::string name, compatibility * com);




private:

    std::string baseDirOne, baseDirTwo, backupCmdAdd, baseDirOne_fsType, baseDirTwo_fsType;
    bool argBackup, argVerbose, argIncludeHidden, argDateSync, argCheckFilenames;
    std::vector<std::string> foldsToSync, baseDirOne_Dirs, baseDirTwo_Dirs;
    std::vector<std::string> doNotSync;
};

#endif // NSSCP_CORE.H
