/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  GtN <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FRONT_END_H
#define FRONT_END_H

#include <vector>
#include <string>
#include <iostream>

#include "nsscp_core.h"

/**
 * @todo write docs
 */

struct structConfig {
  std::string baseDirOne;
  std::string baseDirTwo;
  bool checkFilenames;
  bool includeHidden, backup, help, verbose,noDateSync;
  
  std::vector<std::string> command;
  std::vector<std::string> arg;
  
  int error;
};

class front_end
{
public:

    static structConfig parse_arguments(int argc, char *argv[]);
    static bool addDir(structConfig * config, std::string dir);
    static bool makeArguments(structConfig config);
    static void showHelp();
    static void showError(std::string err);



};

#endif // FRONT_END_H
