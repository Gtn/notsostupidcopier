#include "header/nsscp_core.h"
#include <unordered_map>

nsscp_core::nsscp_core() {}

int nsscp_core::nsscp(std::string _baseDirOne, std::string _baseDirTwo, bool _argIncludeHidden, bool _argVerbose, bool _argBackup, bool _argCheckFilenames, bool _argDateSync) {

    
	baseDirTwo = _baseDirTwo;
	baseDirOne = _baseDirOne;
	argIncludeHidden = _argIncludeHidden;
	argVerbose = _argVerbose;
	argBackup = _argBackup;
	argCheckFilenames = _argCheckFilenames;
	argDateSync = !(_argDateSync);
    
    if(baseDirTwo[baseDirTwo.size() - 1] == '/') {
        baseDirTwo = baseDirTwo.substr(0, baseDirTwo.size() - 1);
    }

    if(baseDirOne[baseDirOne.size() - 1] == '/') {
        baseDirOne = baseDirOne.substr(0, baseDirOne.size() - 1);
    }
    
    out("Synchronising " + baseDirOne + " and " + baseDirTwo, 'i');
    
//     return 0;
    
	std::vector<std::string> doNotSync;

	std::vector<std::string> foldsToSync;
	std::vector<std::string> baseDirOne_Dirs;
	std::vector<std::string> baseDirTwo_Dirs;

	std::string cmd = "touch /tmp/CleverCopier_commandError";
	backupCmdAdd = argBackup ? " --backup " : " ";

	execCmd(&cmd, true);    

    if(argCheckFilenames) {
        std::vector<compatibility> filenamesCheck;
        std::string fsCompOne = getFilesystemType(&baseDirOne), fsCompTwo = getFilesystemType(&baseDirTwo);
        if(fsCompOne != fsCompTwo) {
            out("checking for compatibility...", 'i');
            filenamesCheck = loadCompatibility(fsCompOne, fsCompTwo);
            if(!checkCompatibility(&filenamesCheck)) {
                out("Filesystem compatibility check failed, aborting", 'e');
                return 1;
            }
        } else {
            out("filesystem are the same, do not checking for compatibility between two same filesystems", 'i');
        }
    }  
    
	int foldSyncErr = folderSync();
	if(foldSyncErr != 0)
        out("Folder synchronisation exited with error : " + std::to_string(foldSyncErr), 'o');
    int fileSyncErr = fileSync();
    if(fileSyncErr != 0)
        out("File synchronisation exited with error : " + std::to_string(fileSyncErr), 'o');

    return 0;
}

std::string nsscp_core::getFilesystemType(std::string * dir) {
    std::string cmd, line, fs = "";
    cmd = "df -T " + *dir + " >> /tmp/nsscp_compatibility_check";
    execCmd(&cmd, false);
    std::ifstream stream ("/tmp/nsscp_compatibility_check");
    if(stream) {
        getline(stream, line);
        getline(stream, line);

        for(int i(15); i < 25; i++) {
            if(line[i] == ' ') {
                break;
            } else {
                fs += line[i];
            }
        }
        cmd = "rm /tmp/nsscp_compatibility_check";
//         execCmd(&cmd, true);
//         out(fs, 'd');
        out(*dir + " type is " + fs + " filesystem", 'i');
        execCmd(&cmd, true);
        return fs;        
    } else {
        out("Cannot read /tmp/nsscp_compatibility_check", 'e');
    }
    cmd = "rm /tmp/nsscp_compatibility_check";
    execCmd(&cmd, false);
    return "";
}

bool nsscp_core::checkCompatibility(std::vector<compatibility> * cptb) {
    
    std::string cmd, line, dir;
    if(argIncludeHidden) {
        cmd = "ls -ARBb1 " + baseDirOne + " > /tmp/CleverCopier_1.dat";
    } else {
        cmd = "ls -RBb1 " + baseDirOne + " > /tmp/CleverCopier_1.dat";
    }
    execCmd(&cmd, false);
    
    std::ifstream stream("/tmp/CleverCopier_1.dat");
    
    if(!stream) {
        out("Cannot read /tmp/CleverCopier_1.dat", 'e');
        return false;
    }
    
    std::string range("\\ ");
    
    bool jump = true, err = false;
    while(getline(stream, line)) {
        boost::replace_all(line, "\\\\", "\\");
        boost::replace_all(line, "\\ ", " ");
//         out("line : " + line, 'd');
        if(line == "") {
            jump = true;
            continue;
        }
        if(jump) {
            dir = line;
            jump = false;
            continue;
        }
//         out("LINE : " +#include <boost/algorithm/string/trim_all.hpp> line, 'd');
        if(!checkNameCompatiblity(line, &(cptb->at(1)))) {
            out(line + " in " + dir + " is not compatible with " + cptb->at(1).name , 'i');
            err = true;
        }
    }
    
    stream.close();
    
    cmd = "rm /tmp/CleverCopier_1.dat && touch /tmp/CleverCopier_1.dat";
    execCmd(&cmd, true);
    

    if(argIncludeHidden) {
        cmd = "ls -ARBb1 " + baseDirTwo + " > /tmp/CleverCopier_1.dat";
    } else {
        cmd = "ls -RBb1 " + baseDirTwo + " > /tmp/CleverCopier_1.dat";
    }
    
    execCmd(&cmd, false);
    
    
    stream.open("/tmp/CleverCopier_1.dat");
    
    if(!stream) {
        out("Cannot read /tmp/CleverCopier_1.dat", 'e');
        return false;
    }
    
    jump = true;
    
    while(getline(stream, line)) {
        boost::replace_all(line, "\\\\", "\\");
        boost::replace_all(line, "\\ ", " ");
        if(line == "") {    // This mean that the next line is the new directory ls will look in
            jump = true;
            continue;
        }
        if(jump) {      // This mean that we are at the new directory
            dir = line;
            jump = false;
            continue;
        }
//         out("LINE : " + line, 'd');
        if(!checkNameCompatiblity(line, &(cptb->at(0)))) {
            out(line + " in " + dir + " is not compatible with " + cptb->at(0).name , 'i');
            err = true;
        }
    }
        
    stream.close();
    
    cmd = "rm /tmp/CleverCopier_1.dat && touch /tmp/CleverCopier_1.dat";
    execCmd(&cmd, true);
    
    if(err)
        return false;
    
    return true;
}

bool nsscp_core::checkNameCompatiblity(std::string name, compatibility * com) {
    out("checking : " + name + " with " +  com->name + " filesystem", 'd'); 
//     out("name : " + name, 'd');
    if(name.find_first_of(com->unallowed_char) < name.size()) {
//         out("unallowed char " + name, 'd');
        return false;
    }
    if(com->max_lenght < name.size()) {
//         out("max lengh : " + std::to_string(com->max_lenght), 'd');
        return false;
    }
    if(!(com->end_space_allowed) && (name.at(name.size() - 1) == ' ')) {

//         out("end space", 'd');
        return false;
    }
    for(auto a:com->unallowed_word) {
        if(name.find(a) < name.size()) {
//             out("unallowed word", 'd');
            return false;
        }
    }
    return true;
}

// COMMAND : ls -R1


std::vector<compatibility> nsscp_core::loadCompatibility(std::string dirOneFS, std::string dirTwoFS) {
    std::string home = getenv("HOME"), line;
    std::ifstream compatibilityCheckFile(home + "/.cache/nsscp.d/compatibility_check");
    std::vector<compatibility> compatibility_vector;
    
    bool error(false);
    
    if(compatibilityCheckFile){
        while(getline(compatibilityCheckFile, line)) {
            if(line.substr(0, 14) == "unallowed_char") {
//                 out("unallowed_char", 'd');
                if(compatibility_vector.size() == 0) {
                    error = true;
                    break;
                }
                compatibility_vector[compatibility_vector.size() - 1].unallowed_char = line.substr(15, line.size() - 15);
                continue;
            }
            if(line.substr(0, 17) == "end_space_allowed") {
//                 out("en_space", 'd');
                if(compatibility_vector.size() == 0) {
                    error = true;
                    break;
                }
                compatibility_vector[compatibility_vector.size() - 1].end_space_allowed = line.substr(18, line.size() - 18) == "true" ? true : false;
                continue;
            }
            if(line.substr(0, 14) == "unallowed_word") {
//                 out("unallowed_word", 'd');
                if(compatibility_vector.size() == 0) {
                    error = true;
                    break;
                }
                line = line.substr(15, line.size() - 15);
                boost::split(compatibility_vector[compatibility_vector.size() - 1].unallowed_word, line, boost::is_any_of(" "));
                continue;
            }
            if(line.substr(0, 10) == "max_lenght") {
//                 out("maxlength", 'd');
                if(compatibility_vector.size() == 0) {
                    error = true;
                    break;
                }
                compatibility_vector[compatibility_vector.size() - 1].max_lenght = std::stoi(line.substr(11, line.size() - 11));
                continue;
            }
//             out("newname", 'd');
            compatibility_vector.push_back(compatibility());
            compatibility_vector[compatibility_vector.size() - 1].name = line;
        
        }
        if(error) {
            out("Error while reading compatibility file in ~/.cache/nsscp.d/compatibility_check", 'e');
            std::vector<compatibility> t;
            return t;
        }    
    }
    else {
        out("can't read compatibility file", 'e');
    }
    
    if(argVerbose) {
        out("===========", ' ');
        for(auto a:compatibility_vector) {
            std::cout << "DEBUG : " << a.name << " " << a.unallowed_char << " " << std::to_string(a.max_lenght) << " " << a.end_space_allowed;
            for(auto b:a.unallowed_word) {
                std::cout << " " << b; 
            }
            std::cout << std::endl;
        }    
        out("===========", ' ');
    }
    compatibility compaBuff;
    
    std::vector<compatibility> compatibility_return;
    
    bool foundOne = false, foundTwo = false;
    
    for(auto a:compatibility_vector) {
        if(a.name == dirOneFS) {
            compatibility_return.push_back(a);
            foundOne = true;
            if(foundTwo) {
                compatibility_return.push_back(compaBuff);
                return compatibility_return;
            }
        }
        if(a.name == dirTwoFS) {
            foundTwo = true;
            if(foundOne) {
                compatibility_return.push_back(a);
                return compatibility_return;
            }
            compaBuff = a;
        }
    }
    
    std::string fs("");
    
    if(foundOne) {
        out("Compatibility found for " + dirOneFS, 'i');
    } else {
        fs += (dirOneFS + " ");
    }
        
    if(foundTwo) {
        out("Compatibility found for " + dirTwoFS, 'i');
    } else {
        fs += (dirTwoFS + " ");
    }
    if(fs != ""){
        out("Compatibility specifications not found for filesystem : " + fs + "\n(can be configured in ~/.cache/nsscp.d/compatibility)", 'e');    
    }
    
    return compatibility_return;
}

int nsscp_core::execCmd(std::string *command, bool redirectError) {
	// std::cout << "COMMAND : " << *command << std::endl;
// 	system("> /tmp/CleverCopier_commandError");
	if(redirectError) {
		*command += " 2> /tmp/CleverCopier_commandError";
	}
	
    if(argVerbose) {
        std::cout << "COMMAND : " + *command << std::endl;
    }
    
	char charCmd[command->length()];
	strcpy(charCmd, command->c_str());

	std::ifstream cmdError("/tmp/CleverCopier_commandError");

	std::string buffLine;

	while(getline(cmdError, buffLine))
		std::cout << "ERROR : " << buffLine << std::endl;

	return system(charCmd);
}

int nsscp_core::folderSync() {
  
	std::string cmd;
	int lsErr;

	// Accessing to folder data

	if(argIncludeHidden) {
		cmd = "ls -gRAGQ --full-time " + baseDirOne + " | grep \\\": > /tmp/CleverCopier_1.dat";
		lsErr = execCmd(&cmd, false);
		cmd = "ls -gRAGQ --full-time " + baseDirTwo + " | grep \\\": > /tmp/CleverCopier_2.dat";
		lsErr += execCmd(&cmd, false);
	} else {
		cmd = "ls -gRGQ --full-time " + baseDirOne + " | grep \\\": > /tmp/CleverCopier_1.dat";
		lsErr = execCmd(&cmd, false);
		cmd = "ls -gRGQ --full-time " + baseDirTwo + " | grep \\\": > /tmp/CleverCopier_2.dat";
		lsErr += execCmd(&cmd, false);
	}

	if(lsErr != 0){

		out("Failed to list all subdirectories (ls error)", 'e');
		return 1;
	}

	//Converting text data into vectors

	std::ifstream baseDirOne_ListResult("/tmp/CleverCopier_1.dat"), baseDirTwo_ListResult("/tmp/CleverCopier_2.dat");

	if(!baseDirOne_ListResult.is_open() || !baseDirTwo_ListResult.is_open()) {
		out("Failed to open temporary files (in /tmp/CleverCopier_*.dat", 'e');
		return 1;
	}

	std::string buffLine("");

	std::string baseDirOne_AllFile("");
	std::string baseDirTwo_AllFile("");


	getline(baseDirOne_ListResult, buffLine);
	getline(baseDirTwo_ListResult, buffLine);
	baseDirOne_Dirs.push_back("/");
	baseDirTwo_Dirs.push_back("/");


	while(getline(baseDirOne_ListResult, buffLine)) {
		baseDirOne_Dirs.push_back("/" + buffLine.substr(baseDirOne.size() + 2 , buffLine.size() - 4 - baseDirOne.size()));
	}

	while(getline(baseDirTwo_ListResult, buffLine)) {
		baseDirTwo_Dirs.push_back("/" + buffLine.substr(baseDirTwo.size() + 2, buffLine.size() - 4 - baseDirTwo.size()));
	}


	//If debug is on, will list all directories

	// out("baseDirOne_Dirs", 'd');
	// for(auto a:baseDirOne_Dirs) out(a, 'd');

	// out("baseDirTwo_Dirs", 'd');
	// for(auto a:baseDirTwo_Dirs) out(a, 'd');

	unsigned short int i(0), j(0);
    
	//Copying folders

	while(notLast(&i, &j)) { 	// While i and j aren't at the end of folders list, continue

		// out(std::to_string(i) + " " + std::to_string(j), 'd');
	
		if(baseDirOne_Dirs[i] == baseDirTwo_Dirs[j]) {	// If the folders are the same, continue without doing anything, but add it to foldsToSync
														// Later, the file synchroniser will look in this folders for files to sync
			foldsToSync.push_back(baseDirOne_Dirs[i]);
			out("folds to sync + " + baseDirOne_Dirs[i], 'd');
			i++; j++; 
			continue;

		} else {	// If the folds aren't the same, we just look if : the folds are presents, but farer in the fold list, so we don't copy it

			bool u = std::find(baseDirTwo_Dirs.begin(), baseDirTwo_Dirs.end(), baseDirOne_Dirs[i]) == baseDirTwo_Dirs.end();
			bool v = std::find(baseDirOne_Dirs.begin(), baseDirOne_Dirs.end(), baseDirTwo_Dirs[j]) == baseDirOne_Dirs.end();

			if(u) { // file from first list is not found in the second list, so we copy it to the second file
				cmd = "cp -r '" + baseDirOne + baseDirOne_Dirs[i] + "' '" + baseDirTwo + getUpperFolder( &baseDirOne_Dirs[i]) +  "'";  
				execCmd(&cmd, true);
				doNotSync.push_back(baseDirOne_Dirs[i]);
				out("doNotSync + " + baseDirOne_Dirs[i], 'd');
				i++;
			}
			if(v) {		// file from first list is not foudn in the second list, so we copy it to first file
				cmd = "cp -r '" + baseDirTwo + baseDirTwo_Dirs[j] + "' '" + baseDirOne + getUpperFolder(& baseDirTwo_Dirs[j]) + "'";  
				execCmd(&cmd, true);
				doNotSync.push_back(baseDirTwo_Dirs[j]);
				out("doNotSync + " + baseDirTwo_Dirs[j], 'd');
				j++;
			} else {
				if(!v && !u) {
					out("error while processing : " + baseDirOne + baseDirOne_Dirs[i] + " and " + baseDirTwo + baseDirTwo_Dirs[j], 'e');
					i++; j++;
				}
			}
		}
	}

	return 0;

}

std::string nsscp_core::getUpperFolder (std::string * fold) {
	return fold->substr(0, fold->find_last_of("/"));
}

bool nsscp_core::notLast(unsigned short int * i, unsigned short int * j) {
	if((*i) >= baseDirOne_Dirs.size() || (*j) >= baseDirTwo_Dirs.size()) {	// j or i are/is more than last, so we just have to copy last files
		if((*i) >= baseDirOne_Dirs.size()) {		
			if((*j) >= baseDirTwo_Dirs.size()) {
				return false; // i and j are the last, so quit
			} else {
			out("finishFoldCopy : from second to first", 'i');
				finishFoldCopy(j, false);
				return false;	// we just finished to copy last files
			}
		} else {
			out("finishFoldCopy : from first to second", 'i');
			finishFoldCopy(i, true);
			return false;	// we just finished to copy last files
		}
	} else { // Just check if the next file to process is not a child of a doNotSync fold 

		while(checkDoNotSyncList(baseDirOne_Dirs[*i])) {	// Return true if folder is part of doNotSync
			(*i)++;
			if(*i >= baseDirOne_Dirs.size()) {
				out("finishFoldCopy : from second to first", 'i');
				finishFoldCopy(j, false);
				return false;
			}
		}

		while (checkDoNotSyncList(baseDirTwo_Dirs[*j])) {	// Return true if folder is part of doNotSync
			(*j)++;
			if(*j >= baseDirTwo_Dirs.size()) {
				out("finishFoldCopy : from first to second", 'i');
				finishFoldCopy(i, true);
				return false;
			}
		}
	}
	return true;
}

bool nsscp_core::checkDoNotSyncList(std::string dir) {		// Return true if the dir is a subdir of doNotSync directories 
	
	for (unsigned long int i(0); i < doNotSync.size(); i++) {

		// out("substr | NotSync :" + dir + " | " + doNotSync[i], 'd');
		
		if(doNotSync[i] == dir.substr(0, doNotSync[i].size())) {
			return true;
		}
	}
	return false;
}

void nsscp_core::finishFoldCopy(unsigned short int * a, bool firstToSecond) {
	std::string cmd;
	if(firstToSecond) {
		for(; *a < baseDirOne_Dirs.size(); (*a)++) {
			if(!checkDoNotSyncList(baseDirOne_Dirs[*a])){
				cmd = "cp -r '" + baseDirOne + baseDirOne_Dirs[*a] + "' '" + baseDirTwo + getUpperFolder( &baseDirOne_Dirs[*a]) +  "'";  
				execCmd(&cmd, true);
				doNotSync.push_back(baseDirOne_Dirs[*a]);
				out("doNotSync + " + baseDirOne_Dirs[*a], 'd');
			}
		}
	} else {
		for(; *a < baseDirTwo_Dirs.size(); (*a)++) {
			if(!checkDoNotSyncList(baseDirTwo_Dirs[*a])){
				cmd = "cp -r '" + baseDirTwo + baseDirTwo_Dirs[*a] + "' '" + baseDirOne + getUpperFolder(& baseDirTwo_Dirs[*a]) + "'";  
				execCmd(&cmd, true);
				doNotSync.push_back(baseDirTwo_Dirs[*a]);	
				out("doNotSync + " + baseDirTwo_Dirs[*a], 'd');
			}
		}
	}
}

int nsscp_core::fileSync() {
	std::vector<std::string>::iterator foldsToSyncIT{ std::begin(foldsToSync) };
	std::unordered_map<std::string, std::string> baseDirOne_map, baseDirTwo_map;

	while(foldsToSyncIT != std::end(foldsToSync)) {
		baseDirOne_map.clear();
		baseDirTwo_map.clear();
		std::string cmd;
// 		int lsErr;
		// Gettings files

		cmd =  argIncludeHidden ? "ls -gAGB --full-time '" + baseDirOne + *foldsToSyncIT + "' | grep ^- > /tmp/CleverCopier_1.dat" : "ls -gGB --full-time '" + baseDirOne + *foldsToSyncIT + "' | grep ^- > /tmp/CleverCopier_1.dat";
		execCmd(&cmd, false);
		cmd =  argIncludeHidden ? "ls -gAGB --full-time '" + baseDirTwo + *foldsToSyncIT + "' | grep ^- > /tmp/CleverCopier_2.dat" : "ls -gGB --full-time '" + baseDirTwo + *foldsToSyncIT + "' | grep ^- > /tmp/CleverCopier_2.dat";
		execCmd(&cmd, false);		///// If we want to include links, set grep '^-\|^l' > /tmp/.....

		// if(lsErr > 1){
			// out("Failed to list all files of directories " + baseDirOne + *foldsToSyncIT + " or " + baseDirTwo + *foldsToSyncIT + " (ls error)", 'e');
			// return 1;
		// }

		std::ifstream baseDirOne_fileSync("/tmp/CleverCopier_1.dat"), baseDirTwo_fileSync("/tmp/CleverCopier_2.dat");

		if(!baseDirOne_fileSync.is_open() || !baseDirTwo_fileSync.is_open()) {
			out("Failed to open temporary files (in /tmp/CleverCopier_*.dat", 'e');
			return 1;
		}

		convertTextToMap(&baseDirOne_map, &baseDirOne_fileSync);
		convertTextToMap(&baseDirTwo_map, &baseDirTwo_fileSync);

		// out("LIST DIRS", 'd');

		// for(auto &a:baseDirOne_map) {
		// 	out(a.first, 'd');
		// }

		// for(auto &a:baseDirTwo_map) {
		// 	out(a.first, 'd');
		// }

		// out("==========", 'd');
		
		std::unordered_map<std::string, std::string>::iterator baseDirOne_map_it(std::begin(baseDirOne_map)), baseDirTwo_map_it(std::begin(baseDirTwo_map));

		while(baseDirOne_map_it != std::end(baseDirOne_map) || baseDirTwo_map_it != std::end(baseDirTwo_map)) {
			// out("00000", 'd');
			if(baseDirTwo_map_it == std::end(baseDirTwo_map)) {
				// out("finsh copy", 'd');
				// out("11111", 'd');
				while(baseDirOne_map_it != std::end(baseDirOne_map)) {
					//// BACKUP
					cmd = "cp --preserve" + backupCmdAdd + "'" + baseDirOne + *foldsToSyncIT + '/' + (*baseDirOne_map_it).first + "' '" + baseDirTwo + *foldsToSyncIT + "'";
					execCmd(&cmd, true);
					baseDirOne_map_it++;
				}
				// out("_______________", 'd');
				break;
			}

			if(baseDirOne_map_it == std::end(baseDirOne_map)) {
				// out("22222", 'd');

				// out("finsh copy", 'd');
				while(baseDirTwo_map_it != std::end(baseDirTwo_map)) {
					//// BACKUP
					cmd = "cp --preserve" + backupCmdAdd + "'" + baseDirTwo + *foldsToSyncIT + '/' + (*baseDirTwo_map_it).first + "' '" + baseDirOne + *foldsToSyncIT + "'";
					execCmd(&cmd, true);
					baseDirTwo_map_it++;
				}
				// out("_______________", 'd');
				break;
			}
			// out("33333", 'd');

			// out((*baseDirTwo_map_it).first + " | " + (*baseDirOne_map_it).first, 'd');

			if((*baseDirTwo_map_it).first == (*baseDirOne_map_it).first) {
				fileDateSync(baseDirOne_map_it, baseDirTwo_map_it, foldsToSyncIT);

				*baseDirOne_map_it++; *baseDirTwo_map_it++;
				continue;
			} else {
				std::unordered_map<std::string, std::string>::iterator uIt = std::find(baseDirTwo_map.begin(), baseDirTwo_map.end(), *baseDirOne_map_it);
				std::unordered_map<std::string, std::string>::iterator vIt = std::find(baseDirOne_map.begin(), baseDirOne_map.end(), *baseDirTwo_map_it);

				if(uIt != baseDirTwo_map.end() || vIt !=  baseDirOne_map.end()) {
					if(uIt != baseDirTwo_map.end()){
						fileDateSync(uIt, baseDirOne_map_it, foldsToSyncIT);
						baseDirOne_map_it++;
					}
					if(vIt != baseDirOne_map.end()){
						fileDateSync(vIt, baseDirTwo_map_it, foldsToSyncIT);
						baseDirTwo_map_it++;
					}
					continue;
				} else {
                    
					cmd = std::string("cp --preserve") + backupCmdAdd + "'" + baseDirOne + *foldsToSyncIT + "/" + (*baseDirOne_map_it).first + "' '" + baseDirTwo + *foldsToSyncIT + "'";
					execCmd(&cmd, true);
					cmd = "cp --preserve" + backupCmdAdd + "'" + baseDirTwo + *foldsToSyncIT + '/' + (*baseDirTwo_map_it).first + "' '" + baseDirOne + *foldsToSyncIT + "'";
					execCmd(&cmd, true);
					baseDirTwo_map_it++; baseDirOne_map_it++;
					// out("____________\n", 'd');
				}
			}
		}
		foldsToSyncIT++;
	}

	// for(auto &a : baseDirOne_map)
		// out("==> " + a.first + " | " +a.second, 'd');
	return 0;
}

void nsscp_core::fileDateSync(std::unordered_map<std::string, std::string>::iterator uIt, std::unordered_map<std::string, std::string>::iterator vIt, std::vector<std::string>::iterator dir) {
	// out("fileDateSync", 'd');
	if(argDateSync){
		std::string cmd;
		if((*uIt).second > (*vIt).second) {
			////BACKUP
			cmd = "cp --preserve" + backupCmdAdd + "'" + baseDirOne + *dir + '/' + (*uIt).first + "' '" + baseDirTwo + *dir + "'";
			execCmd(&cmd, true);
		}
		if((*uIt).second < (*vIt).second) {
			////BACKUP
			cmd = "cp --preserve" + backupCmdAdd + "'" + baseDirTwo + *dir + '/' + (*vIt).first + "' '" + baseDirOne + *dir + "'";
			execCmd(&cmd, true);
		}
	}	// out("_______________\n", 'd');
	return;
}

void nsscp_core::convertTextToMap(std::unordered_map<std::string, std::string> * map, std::ifstream * file) {
	
	boost::regex re(" +");        // Create the reg exp
	boost::sregex_token_iterator end;

	std::string buffLine, buffName;
	std::vector<std::string> buffVect;

	while(getline(*file, buffLine)) {
		// out(buffLine, 'd');
		buffName = "";
		buffVect.clear();

		// boost::split(buffVect, buffLine, boost::is_any_of(" "));
		

 		boost::sregex_token_iterator p (buffLine.begin(), buffLine.end(), re , -1);  // sequence and that reg exp

   		while(p != end){
   			buffVect.push_back(*p);
   			*p++;
   		}

   		// for(auto a:buffVect)
   		// 	out("||||||" + a, 'd');
		buffLine = buffVect[3] + buffVect[4];
        buffLine = buffLine.substr(0, 18);
	   	for(short unsigned int i(6); i < buffVect.size(); i++) {
   			buffName += (i==6) ? buffVect[i] : (" " + buffVect[i]);

		}

		map->insert({buffName, buffLine});
		buffLine.clear();
	}

	return;
}



template <typename T>
void nsscp_core::out(T command, char type) {
	switch(type) {
		case 'c':
            if(argVerbose) {
                std::cout << "COMMAND : " << command << std::endl;
            }
            return;
		case 'i':
			std::cout << "INFO : " << command << std::endl;
			return;
		case 'd':
            if(argVerbose) {
                std::cout << "DEBUG : " << command << std::endl;
            }
            return;
		case 'e':
			std::cout << "ERROR : " << command << std::endl;
			return;	
		case 'o':
			std::cout << "SYNKEY OUTPUT : " << command << std::endl;
			return;
        default:
            std::cout << command << std::endl;
	}
	return;
}

std::string nsscp_core::loadProfile(std::string name)
{
    std::string home = getenv("HOME");
	std::ifstream profiles(home + "/.cache/nsscp.d/profiles", std::ios::in);
	
    std::string line;
	
    if(profiles)
	{
		while(getline(profiles, line)) {
            if(line == name) {
                getline(profiles, line);
                return line;
            }
            getline(profiles, line);
        }
	}
	else
		return "";
    return "";
}

bool nsscp_core::saveProfile(std::string path, std::string name)
{
    if(name == "")
        std::cout << "ERROR : Sorry, you can't save a path without a name" << std::endl;
    
    std::string home = getenv("HOME");

    std::cout << "INFO : " << "saving path : " << path << " as : " << name << std::endl;
    
// 	out("saving path : " + path + " as : " + name, 'i') ;
    
	std::ofstream profiles(home + "/.cache/nsscp.d/profiles", std::ios::app);
	
    system("touch ~/.cache/nsscp.d/profiles");
    
    std::string exist = loadProfile(name);
	
    if(profiles) {
		if (exist == "")
			profiles << name << "\n" << path << "\n";
		else {
            std::cout << "ERROR : profile already exist with  path : " << loadProfile(name) << std::endl;
			return false;
		}
	}
	else {
        std::cout << "ERROR : Cannot access to profiles file" << std::endl;
        return false;
    }
    return true;
}

bool nsscp_core::listProfiles()
{
    std::string home = getenv("HOME");
	std::ifstream profiles(home + "/.cache/nsscp.d/profiles");
	
    std::string line;
// 	
    if(profiles) {
        std::cout << "profiles (name : path) :" << std::endl; 
		while(getline(profiles, line)) {
            std::cout << line << " : ";
			getline(profiles, line);
			std::cout << line << std::endl;
		}
		return true;
	} else {
		system("touch ~/.cache/nsscp.d/profiles");
        std::cout << "No profiles saved" << std::endl;
        return false;
	}
}

bool nsscp_core::removeProfile(std::string name){
    std::string home = getenv("HOME");
    
	std::ifstream profiles(home + "/.cache/nsscp.d/profiles");
	std::ofstream buff(home + "/.cache/nsscp.d/profilesBuff", std::ios::app);
	
    std::string line;

	unsigned short int found = 0;
	
    if(profiles && buff) {
        while(getline(profiles, line)) {
            if(line == name || found == 1) {
                found++;
            } else {
                buff << line << std::endl;
            }
        }

        profiles.close();
        
        system("mv ~/.cache/nsscp.d/profilesBuff ~/.cache/nsscp.d/profiles");

        if(found != 2) {
            std::cout << "ERROR : Profile not found" << std::endl;
            return false;

        }
        return true;
	} else {
		system("touch ~/.cache/nsscp.d/profiles");
        std::cout << "ERROR : Profile not found" << std::endl;
		return false;
	}
	
}
