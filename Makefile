
CC=g++

CCOPTION = Wall std=gnu++11
FLAGS = $(addprefix -,$(CCOPTION))

OBJ = main.o front_end.o nsscp_core.o 

BOOST_DIR = /usr/local/boost_1_71_0/
BOOST = $(addprefix -I , $(BOOST_DIR))

OBJ_DIR = obj
BUILD_DIR = build
SRC_DIR = src

REGEX = /usr/local/boost_1_71_0/stage/lib/libboost_regex.a

$(BUILD_DIR)/nsscp: $(OBJ_DIR)/main.o $(OBJ_DIR)/nsscp_core.o $(OBJ_DIR)/front_end.o
	g++ $(addprefix $(OBJ_DIR)/,$(OBJ)) -o build/nsscp $(REGEX)

$(OBJ_DIR)/main.o : $(SRC_DIR)/main.cpp
	$(CC) $(FLAGS) -c $(SRC_DIR)/main.cpp -o $(OBJ_DIR)/main.o $(BOOST)

$(OBJ_DIR)/nsscp_core.o : $(SRC_DIR)/nsscp_core.cpp
	$(CC) $(FLAGS) -c $(SRC_DIR)/nsscp_core.cpp -o $(OBJ_DIR)/nsscp_core.o $(BOOST)

$(OBJ_DIR)/front_end.o : $(SRC_DIR)/front_end.cpp
	$(CC) $(FLAGS) -c $(SRC_DIR)/front_end.cpp -o $(OBJ_DIR)/front_end.o $(BOOST)

clean: 
	rm $(OBJ_DIR)/*.o
